#!/bin/bash

#Reading in config vars from env_vars.sh
source ./bin/export_vars.sh

########################
## Installing plugins ##
########################

/bin/bash ./bin/plugin-installer.sh

########################################################
## Loading Elasticsearch Resources from json_configs/ ##
########################################################
log_message(){
  message=${1:-'Logged Message'}
  topic=${2:-'Log'}
  level=${3:-'INFO'}
  echo $(date +%s)" ${topic}[${level}]: ${message}"
}

status_check(){
  attempts=${1:-75}
  while [ ${attempts} -gt 0 ];
    do
      log_message "Remaining attempts: ${attempts}" "ES_HEALTH"
      if [[ $( curl -sS --insecure -u ${user_string} https://${es_host}:${es_port}/_cluster/health 2>> /dev/null | jq .status ) == *"green"* ]];
        then
          log_message "Connection to elasticsearch established" "ES_HEALTH"
          attempts=-1
        fi
    sleep 1
    (( attempts-- ))
    done
}

status_check

config_array=( ${esconfig_files}/* )

for file in "${config_array[@]}";
  do
      cat ${file} | jq .payload > tmp.json

      resp=$( curl --insecure -sS -XPUT -u ${user_string} https://${es_host}:${es_port}/$( cat ${file} | jq -r .meta.endpoint ) \
                $(  cat ${file} | jq -r .meta.method  ) \
                -H "$( cat ${file} | jq -r .meta.app_type )" \
                -d @tmp.json )

      if [[ ${resp} == *true* ]] || [[ ${resp} == *successful\":1* ]];
        then
          log_message "$( cat ${file} | jq .meta.endpoint ) Added to ES" "Endpoint"
        else
          log_message "$( cat ${file} | jq .meta.endpoint ) Failed to add resource" "Endpoint" "ERROR"
        fi

      rm tmp.json
  done

log_message "Script Successfully Completed"
exit 0
