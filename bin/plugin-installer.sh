#!/bin/bash

plugins_array=( $( cat plugin_list.txt ) )

elastic_nodes=( $( docker ps -a | awk '/odfe-node/{print $1}' ) )

for nodes in "${elastic_nodes[@]}";
  do
    for plugins in "${plugins_array[@]}";
      do
        echo 'Node: '${nodes} 'Plugin: '${plugins}
        docker exec --user elasticsearch:root ${nodes} sh -c "yes | ./bin/elasticsearch-plugin install ${plugins}"
      done
  done


docker-compose restart odfe-node1
docker-compose restart odfe-node2

docker-compose exec odfe-node1 ./bin/elasticsearch-plugin list | grep 'ingest'
docker-compose exec odfe-node2 ./bin/elasticsearch-plugin list | grep 'ingest'
